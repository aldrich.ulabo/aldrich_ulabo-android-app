package com.ajru.exercise.contacttracing

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.buttonMain)

        button.setOnClickListener {
            callHealthInfo()
        }
    }

    private fun callHealthInfo() {
        val editTextName = findViewById<EditText>(R.id.editTextName)
        val editTextNumber = findViewById<EditText>(R.id.editTextMobileNumber)
        val editTextCity = findViewById<EditText>(R.id.editTextMobileCity)

        val dataName = editTextName.text.toString()
        val dataNumber = editTextNumber.text.toString()
        val dataCity = editTextCity.text.toString()

        val intent = Intent(this, HealthInfo::class.java).also {
            it.putExtra( "NAME_DATA", dataName)
            it.putExtra("NUMBER_DATA", dataNumber)
            it.putExtra("CITY_DATA", dataCity)
            startActivity(it)
        }
    }
}