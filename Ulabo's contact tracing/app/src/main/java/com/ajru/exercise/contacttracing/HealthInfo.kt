package com.ajru.exercise.contacttracing

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView

class HealthInfo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_info)

        val button = findViewById<Button>(R.id.buttonGuest)
        button.setOnClickListener {
            callConfirmation()
        }

        val dataName = intent.getStringExtra("NAME_DATA")
        val dataNumber = intent.getStringExtra("NUMBER_DATA")
        val dataCity = intent.getStringExtra("CITY_DATA")

    }

    private fun callConfirmation() {
        val tempQ1 = findViewById<TextView>(R.id.textViewQ1Answer)
        val tempQ2 = findViewById<TextView>(R.id.textViewQ2Answer)

        val dataName = intent.getStringExtra("NAME_DATA")
        val dataNumber = intent.getStringExtra("NUMBER_DATA")
        val dataCity = intent.getStringExtra("CITY_DATA")
        val dataQ1 = tempQ1.text.toString()
        val dataQ2 = tempQ2.text.toString()

        val intent = Intent(this, Confirmation::class.java).also {
            it.putExtra( "NAME_DATA", dataName)
            it.putExtra("NUMBER_DATA", dataNumber)
            it.putExtra("CITY_DATA", dataCity)
            it.putExtra("Q1_DATA", dataQ1)
            it.putExtra("Q2_DATA", dataQ2)
            startActivity(it)
        }
    }

    fun onRadiobuttonQuestion1Clicked(view: View) {
        val inputSymptoms = findViewById<EditText>(R.id.inputSymptoms)
        if (view is RadioButton) {
            val checked = view.isChecked

            when (view.getId()) {
                R.id.radioQ1YES ->
                    if (checked) {
                        val textviewQ1 = findViewById<TextView>(R.id.textViewQ1Answer).apply {
                            text = "YES"

                            inputSymptoms.visibility=View.VISIBLE
                        }

                    }
                R.id.radioQ1NO ->
                    if (checked) {
                        val textviewQ1 = findViewById<TextView>(R.id.textViewQ1Answer).apply {
                            text = "NO"
                            inputSymptoms.visibility=View.INVISIBLE
                        }
                    }
            }
        }

    }

    fun onRadiobuttonQuestion2Clicked(view: View) {
        val inputContact = findViewById<EditText>(R.id.inputContactHistory)
        if (view is RadioButton) {
            val checked = view.isChecked

            when (view.getId()) {
                R.id.radioQ2YES ->
                    if (checked) {
                        val textviewQ2 = findViewById<TextView>(R.id.textViewQ2Answer).apply {
                            text = "YES"
                            inputContact.visibility=View.VISIBLE

                        }

                    }
                R.id.radioQ2NO ->
                    if (checked) {
                        val textviewQ2 = findViewById<TextView>(R.id.textViewQ2Answer).apply {
                            text = "NO"
                            inputContact.visibility=View.INVISIBLE
                        }
                    }
            }
        }

    }
}