package com.ajru.exercise.contacttracing

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class Confirmation : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)

        val button = findViewById<Button>(R.id.buttonConfirmation)
        button.setOnClickListener {
            callResult()
        }

        val dataName = intent.getStringExtra("NAME_DATA")
        val dataNumber = intent.getStringExtra("NUMBER_DATA")
        val dataCity = intent.getStringExtra("CITY_DATA")
        val dataQ1 = intent.getStringExtra("Q1_DATA")
        val dataQ2 = intent.getStringExtra("Q2_DATA")

        val textviewName = findViewById<TextView>(R.id.textViewName).apply {
            text = dataName
        }
        val textviewNumber = findViewById<TextView>(R.id.textViewNumber).apply {
            text = dataNumber
        }
        val textviewCity = findViewById<TextView>(R.id.textViewCity).apply {
            text = dataCity
        }

        val textviewQ1 = findViewById<TextView>(R.id.textViewQ1).apply {
            text = dataQ1
        }

        val textviewQ2 = findViewById<TextView>(R.id.textViewQ2).apply {
            text = dataQ2
        }
    }

    private fun callResult() {
        val tempQ1 = findViewById<TextView>(R.id.textViewQ1)
        val tempQ2 = findViewById<TextView>(R.id.textViewQ2)
        val q1 = tempQ1.text.toString()
        val q2 = tempQ2.text.toString()
        if (q1 == "NO" && q2 == "NO") {
            val dataYes = "You are in a good condition, Keep up"
            val intent = Intent(this, Result::class.java).also {
                it.putExtra("RESULT_DATA", dataYes)
                startActivity(it)
            }
        } else {
            val dataNo = "RISKY, you answered a YES, you should isolate yourself!"
            val intent = Intent(this, Result::class.java).also {
                it.putExtra("RESULT_DATA", dataNo)
                startActivity(it)
            }
        }
    }
}