package com.ajru.exercise.contacttracing

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class Result : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val result = intent.getStringExtra("RESULT_DATA")

        val textviewName = findViewById<TextView>(R.id.textViewResult).apply {
            text = result
        }
    }
}